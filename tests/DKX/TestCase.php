<?php

declare(strict_types=1);

namespace DKX;

use DKX\App\Chapters\ChapterTransformer;
use DKX\JsonApi\Manager;
use DKX\JsonApi\Preprocessor\Preprocessor;
use DKX\JsonApi\TransformContext;
use DKX\JsonApiSerializer\Resource\Document;
use DKX\JsonApiTests\App\Books\Book;
use DKX\JsonApiTests\App\Books\BookTransformer;
use DKX\JsonApiTests\App\Users\User;
use DKX\JsonApiTests\App\Users\UserTransformer;
use PHPUnit\Framework\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
	protected static function assertJsonApiDocumentEquals(Document $expected, array $actual): void
	{
		self::assertEquals($expected->toJsonApiData(), $actual);
	}

	protected function createManager(callable $userLoader = null, callable $chaptersLoader = null, callable $bookLoader = null): Manager
	{
		if ($userLoader === null) {
			$userLoader = function (?User $user = null) {
				return $user;
			};
		}

		if ($chaptersLoader === null) {
			$chaptersLoader = function (array $chapters) {
				return $chapters;
			};
		}

		if ($bookLoader === null) {
			$bookLoader = function (Book $book) {
				return $book;
			};
		}

		return new Manager([
			new UserTransformer(),
			new BookTransformer($userLoader, $chaptersLoader),
			new ChapterTransformer($bookLoader, $userLoader),
		]);
	}

	/**
	 * @deprecated
	 * @param object[] $initItems
	 * @param string[] $includes
	 */
	protected function createPreprocessor(Manager $manager, array $initItems, array $includes = []): Preprocessor
	{
		return new Preprocessor($manager, new TransformContext($includes, []), $initItems);
	}
}
