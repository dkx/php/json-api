<?php

declare(strict_types=1);

require_once __DIR__. '/../../vendor/autoload.php';

require_once __DIR__. '/TestCase.php';

require_once __DIR__. '/App/Users/User.php';
require_once __DIR__. '/App/Users/UserTransformer.php';
require_once __DIR__. '/App/Books/Book.php';
require_once __DIR__. '/App/Books/BookTransformer.php';
require_once __DIR__. '/App/Chapters/Chapter.php';
require_once __DIR__. '/App/Chapters/ChapterTransformer.php';
