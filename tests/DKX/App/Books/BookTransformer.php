<?php

declare(strict_types=1);

namespace DKX\JsonApiTests\App\Books;

use DKX\JsonApi\Item;
use DKX\JsonApi\TransformContext;
use DKX\JsonApi\Transformer;

final class BookTransformer implements Transformer
{
	/** @var callable */
	private $userLoader;

	/** @var callable */
	private $chaptersLoader;

	public function __construct(callable $userLoader, callable $chaptersLoader)
	{
		$this->userLoader = $userLoader;
		$this->chaptersLoader = $chaptersLoader;
	}

	public function supports(object $item): bool
	{
		return $item instanceof Book;
	}

	public function transform(object $book, TransformContext $ctx): Item
	{
		if (!$book instanceof Book) {
			throw new \Exception();
		}

		return new Item('book', $book->getId(), [
			Item::SELF_LINK => '/books/'. $book->getId(),
			Item::ATTRIBUTES => [
				'name' => $book->getName(),
			],
			Item::RELATIONSHIPS => [
				'author' => \call_user_func($this->userLoader, $book->getAuthor()),
				'editor' => \call_user_func($this->userLoader, $book->getEditor()),
				'chapters' => \call_user_func($this->chaptersLoader, $book->getChapters()),
			],
		]);
	}
}
