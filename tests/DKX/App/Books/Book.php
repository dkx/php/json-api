<?php

declare(strict_types=1);

namespace DKX\JsonApiTests\App\Books;

use DKX\JsonApiTests\App\Chapters\Chapter;
use DKX\JsonApiTests\App\Users\User;

final class Book
{
	/** @var string */
	private $id;

	/** @var string */
	private $name;

	/** @var User */
	private $author;

	/** @var User|null */
	private $editor;

	/** @var Chapter[] */
	private $chapters;

	/**
	 * @param Chapter[] $chapters
	 */
	public function __construct(string $id, string $name, User $author, array $chapters = [])
	{
		$this->id = $id;
		$this->name = $name;
		$this->author = $author;
		$this->chapters = $chapters;

		foreach ($chapters as $chapter) {
			$chapter->setBook($this);
		}
	}

	public function getId(): string
	{
		return $this->id;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function getAuthor(): User
	{
		return $this->author;
	}

	public function getEditor(): ?User
	{
		return $this->editor;
	}

	public function setEditor(User $editor): void
	{
		$this->editor = $editor;
	}

	/**
	 * @return Chapter[]
	 */
	public function getChapters(): array
	{
		return $this->chapters;
	}
}
