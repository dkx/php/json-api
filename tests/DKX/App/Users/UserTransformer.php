<?php

declare(strict_types=1);

namespace DKX\JsonApiTests\App\Users;

use DKX\JsonApi\Item;
use DKX\JsonApi\TransformContext;
use DKX\JsonApi\Transformer;

final class UserTransformer implements Transformer
{
	public function supports(object $item): bool
	{
		return $item instanceof User;
	}

	public function transform(object $user, TransformContext $ctx): Item
	{
		if (!$user instanceof User) {
			throw new \Exception();
		}

		return new Item('user', $user->getId(), [
			Item::ATTRIBUTES => [
				'name' => $user->getName(),
			],
		]);
	}
}
