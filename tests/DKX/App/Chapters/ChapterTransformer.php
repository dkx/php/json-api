<?php

declare(strict_types=1);

namespace DKX\App\Chapters;

use DKX\JsonApi\Item;
use DKX\JsonApi\TransformContext;
use DKX\JsonApi\Transformer;
use DKX\JsonApiTests\App\Chapters\Chapter;

final class ChapterTransformer implements Transformer
{
	/** @var callable */
	private $bookLoader;

	/** @var callable */
	private $userLoader;

	public function __construct(callable $bookLoader, callable $userLoader)
	{
		$this->bookLoader = $bookLoader;
		$this->userLoader = $userLoader;
	}

	public function supports(object $item): bool
	{
		return $item instanceof Chapter;
	}

	public function transform(object $chapter, TransformContext $ctx): Item
	{
		if (!$chapter instanceof Chapter) {
			throw new \Exception();
		}

		return new Item('chapter', $chapter->getId(), [
			Item::ATTRIBUTES => [
				'title' => $chapter->getTitle(),
			],
			Item::RELATIONSHIPS => [
				'book' => \call_user_func($this->bookLoader, $chapter->getBook()),
				'author' => \call_user_func($this->userLoader, $chapter->getAuthor()),
			],
		]);
	}
}
