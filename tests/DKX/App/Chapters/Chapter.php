<?php

declare(strict_types=1);

namespace DKX\JsonApiTests\App\Chapters;

use DKX\JsonApiTests\App\Books\Book;
use DKX\JsonApiTests\App\Users\User;

final class Chapter
{
	/** @var string */
	private $id;

	/** @var string */
	private $title;

	/** @var Book */
	private $book;

	/** @var User */
	private $author;

	public function __construct(string $id, string $title, User $author)
	{
		$this->id = $id;
		$this->title = $title;
		$this->author = $author;
	}

	public function getId(): string
	{
		return $this->id;
	}

	public function getTitle(): string
	{
		return $this->title;
	}

	public function getBook(): Book
	{
		return $this->book;
	}

	public function setBook(Book $book): void
	{
		$this->book = $book;
	}

	public function getAuthor(): User
	{
		return $this->author;
	}
}
