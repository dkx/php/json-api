<?php

declare(strict_types=1);

namespace DKX\JsonApiTests;

use DKX\JsonApi\Batch;
use DKX\JsonApi\Deferred;
use DKX\JsonApiSerializer\Relationship\CollectionRelationship;
use DKX\JsonApiSerializer\Relationship\ItemRelationship;
use DKX\JsonApiSerializer\Relationship\NullRelationship;
use DKX\JsonApiSerializer\Resource\Document;
use DKX\JsonApiSerializer\Resource\Item;
use DKX\JsonApiTests\App\Books\Book;
use DKX\JsonApiTests\App\Chapters\Chapter;
use DKX\JsonApiTests\App\Users\User;
use DKX\TestCase;

final class ManagerTest extends TestCase
{
	/** @var Book */
	private $book;

	public function setUp(): void
	{
		parent::setUp();

		$author = new User('5', 'John Doe');

		$this->book = new Book('1', 'A', $author, [
			new Chapter('30', '1st chapter', $author),
			new Chapter('31', '2nd chapter', $author),
			new Chapter('32', '3rd chapter', $author),
		]);
	}

	/**
	 * @dataProvider loaderProvider
	 */
	public function testItemToArray_with_relationship_item(callable $userLoader): void
	{
		$manager = $this->createManager($userLoader);
		$book = $manager->itemToArray($this->book, ['author']);

		self::assertJsonApiDocumentEquals(new Document(
			new Item('book', '1', [
				Item::SELF_LINK => '/books/1',
				Item::ATTRIBUTES => [
					'name' => 'A',
				],
				Item::RELATIONSHIPS => [
					'author' => new ItemRelationship(
						new Item('user', '5', [
							Item::ATTRIBUTES => [
								'name' => 'John Doe',
							],
						])
					),
				],
			])
		), $book);
	}

	/**
	 * @dataProvider loaderProvider
	 */
	public function testItemToArray_with_relationship_null(callable $userLoader): void
	{
		$manager = $this->createManager($userLoader);
		$book = $manager->itemToArray($this->book, ['editor']);

		self::assertJsonApiDocumentEquals(new Document(
			new Item('book', '1', [
				Item::SELF_LINK => '/books/1',
				Item::ATTRIBUTES => [
					'name' => 'A',
				],
				Item::RELATIONSHIPS => [
					'editor' => new NullRelationship(),
				],
			])
		), $book);
	}

	/**
	 * @dataProvider loaderProvider
	 */
	public function testItemToArray_with_relationship_collection(callable $userLoader, callable $chaptersLoader): void
	{
		$manager = $this->createManager($userLoader, $chaptersLoader);
		$book = $manager->itemToArray($this->book, ['chapters']);

		self::assertJsonApiDocumentEquals(new Document(
			new Item('book', '1', [
				Item::SELF_LINK => '/books/1',
				Item::ATTRIBUTES => [
					'name' => 'A',
				],
				Item::RELATIONSHIPS => [
					'chapters' => new CollectionRelationship([
						new ItemRelationship(
							new Item('chapter', '30', [
								Item::ATTRIBUTES => [
									'title' => '1st chapter',
								],
							])
						),
						new ItemRelationship(
							new Item('chapter', '31', [
								Item::ATTRIBUTES => [
									'title' => '2nd chapter',
								],
							])
						),
						new ItemRelationship(
							new Item('chapter', '32', [
								Item::ATTRIBUTES => [
									'title' => '3rd chapter',
								],
							])
						),
					]),
				],
			])
		), $book);
	}

	/**
	 * @dataProvider loaderProvider
	 */
	public function testItemToArray_with_inner_relationship(callable $userLoader, callable $chaptersLoader): void
	{
		$manager = $this->createManager($userLoader, $chaptersLoader);
		$book = $manager->itemToArray($this->book, ['chapters', 'chapters.author']);

		$author = new ItemRelationship(
			new Item('user', '5', [
				Item::ATTRIBUTES => [
					'name' => 'John Doe',
				],
			])
		);

		self::assertJsonApiDocumentEquals(new Document(
			new Item('book', '1', [
				Item::SELF_LINK => '/books/1',
				Item::ATTRIBUTES => [
					'name' => 'A',
				],
				Item::RELATIONSHIPS => [
					'chapters' => new CollectionRelationship([
						new ItemRelationship(
							new Item('chapter', '30', [
								Item::ATTRIBUTES => [
									'title' => '1st chapter',
								],
								Item::RELATIONSHIPS => [
									'author' => $author,
								],
							])
						),
						new ItemRelationship(
							new Item('chapter', '31', [
								Item::ATTRIBUTES => [
									'title' => '2nd chapter',
								],
								Item::RELATIONSHIPS => [
									'author' => $author,
								],
							])
						),
						new ItemRelationship(
							new Item('chapter', '32', [
								Item::ATTRIBUTES => [
									'title' => '3rd chapter',
								],
								Item::RELATIONSHIPS => [
									'author' => $author,
								],
							])
						),
					]),
				],
			])
		), $book);
	}

	public function testItemToArray_with_inner_deferred_relationship(): void
	{
		$actions = [];

		$manager = $this->createManager(function (?User $user = null) use (&$actions) {
			return function () use (&$actions, $user) {
				$actions[] = 'user:prefetch';
				return new Deferred(function () use (&$actions, $user) {
					$actions[] = 'user:fetch';
					return $user;
				});
			};
		}, function (array $chapters) use (&$actions) {
			return function () use (&$actions, $chapters) {
				$actions[] = 'chapters:prefetch';
				return new Deferred(function () use (&$actions, $chapters) {
					$actions[] = 'chapters:fetch';
					return $chapters;
				});
			};
		}, function (Book $book) use (&$actions) {
			return function () use (&$actions, $book) {
				$actions[] = 'book:prefetch';
				return new Deferred(function () use (&$actions, $book) {
					$actions[] = 'book:fetch';
					return $book;
				});
			};
		});

		$manager->itemToArray($this->book, ['author', 'editor', 'chapters.author', 'chapters.book.author', 'chapters.book.editor']);

		self::assertEquals([
			// 1st cycle
			'user:prefetch',
			'user:prefetch',
			'chapters:prefetch',
			'user:fetch',
			'user:fetch',
			'chapters:fetch',
			// 2nd cycle
			'book:prefetch',
			'user:prefetch',
			'book:prefetch',
			'user:prefetch',
			'book:prefetch',
			'user:prefetch',
			'book:fetch',
			'user:fetch',
			'book:fetch',
			'user:fetch',
			'book:fetch',
			'user:fetch',
			// 3rd cycle
			'user:prefetch',
			'user:prefetch',
			'user:prefetch',
			'user:prefetch',
			'user:prefetch',
			'user:prefetch',
			'user:fetch',
			'user:fetch',
			'user:fetch',
			'user:fetch',
			'user:fetch',
			'user:fetch',
		], $actions);
	}

	public function testItemToArray_with_inner_batch_relationship(): void
	{
		$actions = [];

		$manager = $this->createManager(function (?User $user = null) use (&$actions) {
			return function () use (&$actions, $user) {
				return new Batch('users', '', function (array $ids) use (&$actions) {
					$actions[] = 'user:prefetch';
					return [];
				}, function (array $users) use (&$actions, $user) {
					$actions[] = 'user:fetch';
					return $user;
				});
			};
		}, function (array $chapters) use (&$actions) {
			return function () use (&$actions, $chapters) {
				return new Batch('chapters', '', function (array $ids) use (&$actions) {
					$actions[] = 'chapters:prefetch';
					return [];
				}, function (array $ch) use (&$actions, $chapters) {
					$actions[] = 'chapters:fetch';
					return $chapters;
				});
			};
		}, function (Book $book) use (&$actions) {
			return function () use (&$actions, $book) {
				return new Batch('books', $book->getId(), function (array $ids) use (&$actions) {
					$actions[] = 'book:prefetch';
					return [];
				}, function (array $books) use (&$actions, $book) {
					$actions[] = 'book:fetch';
					return $book;
				});
			};
		});

		$manager->itemToArray($this->book, ['author', 'editor', 'chapters.author', 'chapters.book.author', 'chapters.book.editor']);

		self::assertEquals([
			// 1st cycle
			'user:prefetch',
			'user:fetch',
			'user:fetch',
			'chapters:prefetch',
			'chapters:fetch',
			// 2nd cycle
			'book:prefetch',
			'book:fetch',
			'user:prefetch',
			'user:fetch',
			'book:fetch',
			'user:fetch',
			'book:fetch',
			'user:fetch',
			// 3rd cycle
			'user:prefetch',
			'user:fetch',
			'user:fetch',
			'user:fetch',
			'user:fetch',
			'user:fetch',
			'user:fetch',
		], $actions);
	}

	public function loaderProvider(): array
	{
		return [
			'simple' => [
				function (?User $user = null) {
					return $user;
				},
				function (array $chapters) {
					return $chapters;
				},
			],
			'lazy' => [
				function (?User $user = null) {
					return function () use ($user) {
						return $user;
					};
				},
				function (array $chapters) {
					return function () use ($chapters) {
						return $chapters;
					};
				},
			],
			'deferred' => [
				function (?User $user = null) {
					return function () use ($user) {
						return new Deferred(function () use ($user) {
							return $user;
						});
					};
				},
				function (array $chapters) {
					return function () use ($chapters) {
						return new Deferred(function () use ($chapters) {
							return $chapters;
						});
					};
				},
			],
		];
	}
}
