<?php

declare(strict_types=1);

namespace DKX\JsonApiTests;

use DKX\JsonApi\Exception\InvalidStateException;
use DKX\JsonApi\TransformContext;
use DKX\JsonApiTests\App\Books\Book;
use DKX\JsonApiTests\App\Chapters\Chapter;
use DKX\JsonApiTests\App\Users\User;
use DKX\TestCase;

final class ItemTest extends TestCase
{
	/** @var Book */
	private $book;

	public function setUp(): void
	{
		parent::setUp();

		$author = new User('5', 'John Doe');

		$this->book = new Book('1', 'A', $author, [
			new Chapter('30', '1st chapter', $author),
			new Chapter('31', '2nd chapter', $author),
			new Chapter('32', '3rd chapter', $author),
		]);
	}

	public function testGetters(): void
	{
		$ctx = new TransformContext([]);
		$manager = $this->createManager();
		$item = $manager->createItem($ctx, $this->book);

		self::assertSame('book', $item->getType());
		self::assertSame('1', $item->getId());
		self::assertSame('/books/1', $item->getSelfLink());
		self::assertEquals([
			'name' => 'A',
		], $item->getAttributes());
	}

	public function testFetchRelationships_not_prefetched(): void
	{
		$this->expectException(InvalidStateException::class);
		$this->expectExceptionMessage('Can not fetch relationships because they were not prefetched yet');

		$ctx = new TransformContext([]);
		$manager = $this->createManager();
		$item = $manager->createItem($ctx, $this->book);

		$item->fetchRelationships();
	}

	public function testToJsonApiItem_not_fetched(): void
	{
		$this->expectException(InvalidStateException::class);
		$this->expectExceptionMessage('Can not transform item to json api item because relationships are not fetched yet');

		$ctx = new TransformContext([]);
		$manager = $this->createManager();
		$item = $manager->createItem($ctx, $this->book);

		$item->toJsonApiItem();
	}
}
