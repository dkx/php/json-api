.PHONY: install-deps
install-deps:
	docker run --rm -v `pwd`:/app -w /app composer install

.PHONY: test
test:
	docker run --rm -v `pwd`:/app -w /app php:7.3-alpine vendor/bin/phpunit tests/DKX/Tests

.PHONY: phpstan
phpstan:
	docker run --rm -v `pwd`:/app -w /app php:7.3-alpine vendor/bin/phpstan analyze --no-progress -c phpstan.neon
