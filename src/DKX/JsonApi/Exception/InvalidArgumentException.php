<?php

declare(strict_types=1);

namespace DKX\JsonApi\Exception;

final class InvalidArgumentException extends \InvalidArgumentException
{
}
