<?php

declare(strict_types=1);

namespace DKX\JsonApi\Exception;

final class InvalidStateException extends \LogicException
{
}
