<?php

declare(strict_types=1);

namespace DKX\JsonApi\Exception;

final class UnknownTransformerException extends \LogicException {}
