<?php

declare(strict_types=1);

namespace DKX\JsonApi;

use DKX\JsonApi\Exception\UnknownTransformerException;
use DKX\JsonApiSerializer as Serializer;

final class Manager
{
	/** @var Serializer\Serializer */
	private $serializer;

	/** @var Transformer[] */
	private $transformers;

	/**
	 * @param Transformer[] $transformers
	 */
	public function __construct(array $transformers = [])
	{
		$this->serializer = new Serializer\Serializer;
		$this->transformers = $transformers;
	}

	public function addTransformer(Transformer $transformer): void
	{
		$this->transformers[] = $transformer;
	}

	/**
	 * @internal
	 */
	public function createItem(TransformContext $ctx, object $item): Item
	{
		return $this->getTransformer($item)->transform($item, $ctx);
	}

	/**
	 * @param string[] $includes
	 * @param mixed[] $meta
	 * @param mixed[] $contextAttributes
	 * @return mixed[]
	 */
	public function itemToArray(object $item, array $includes = [], array $meta = [], array $contextAttributes = []): array
	{
		$ctx = new TransformContext($includes, $contextAttributes);
		$items = $this->processItems($ctx, [
			$this->createItem($ctx, $item),
		]);

		$doc = new Serializer\Resource\Document($items[0]);

		if (count($meta) > 0) {
			$doc->setMeta(new Serializer\Resource\Meta($meta));
		}

		return $this->serializer->serialize($doc);
	}

	/**
	 * @param mixed[] $items
	 * @param string[] $include
	 * @param mixed[] $meta
	 * @param mixed[] $contextAttributes
	 * @return mixed[]
	 */
	public function collectionToArray(array $items, array $include = [], array $meta = [], array $contextAttributes = []): array
	{
		$ctx = new TransformContext($include, $contextAttributes);
		$items= $this->processItems($ctx, \array_map(function (object $item) use ($ctx) {
			return $this->createItem($ctx, $item);
		}, $items));

		$doc = new Serializer\Resource\Document(
			new Serializer\Resource\Collection($items)
		);

		if (count($meta) > 0) {
			$doc->setMeta(new Serializer\Resource\Meta($meta));
		}

		return $this->serializer->serialize($doc);
	}

	/**
	 * @param Item[] $items
	 * @return mixed[]
	 */
	private function processItems(TransformContext $ctx, array $items): array
	{
		$this->fetchRelationships($ctx, [
			'' => $items,
		]);

		return \array_map(function (Item $item) {
			return $item->toJsonApiItem();
		}, $items);
	}

	/**
	 * @param Item[][] $itemsGroups
	 */
	private function fetchRelationships(TransformContext $ctx, array $itemsGroups): void
	{
		foreach ($itemsGroups as $path => $items) {
			foreach ($items as $item) {
				$includes = $this->getIncludesForPath($path, $ctx->getIncludes());
				$item->prefetchRelationships($this, $ctx, $includes);
			}
		}

		$nextCycleFetch = [];
		foreach ($itemsGroups as $path => $items) {
			foreach ($items as $item) {
				$dependentRelationships = $item->fetchRelationships();
				foreach ($dependentRelationships as $key => $relationship) {
					$nextPath = $path === '' ? $key : ($path. '.'. $key);
					if (!\array_key_exists($nextPath, $nextCycleFetch)) {
						$nextCycleFetch[$nextPath] = [];
					}

					$nextCycleFetch[$nextPath] = \array_merge($nextCycleFetch[$nextPath], $relationship);
				}
			}
		}

		Batch::reset();

		if (count($nextCycleFetch) > 0) {
			$this->fetchRelationships($ctx, $nextCycleFetch);
		}
	}

	/**
	 * @param string[] $allIncludes
	 * @return string[]
	 */
	private function getIncludesForPath(string $path, array $allIncludes): array
	{
		$quotedPath = \preg_quote($path, '/');
		$includes = [];

		foreach ($allIncludes as $include) {
			if ($path === '') {
				$includes[] = \explode('.', $include)[0];
				continue;
			}

			if (\preg_match('/^'. $quotedPath. '\.([a-zA-Z0-9-_]+)/', $include, $m) === 1) {
				$includes[] = $m[1];
			}
		}

		return \array_unique($includes);
	}

	private function getTransformer(object $item): Transformer
	{
		foreach ($this->transformers as $transformer) {
			if ($transformer->supports($item)) {
				return $transformer;
			}
		}

		throw new UnknownTransformerException('Transformer for "'. \get_class($item). '" does not exists');
	}
}
