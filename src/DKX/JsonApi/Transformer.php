<?php

declare(strict_types=1);

namespace DKX\JsonApi;

interface Transformer
{
	public function supports(object $item): bool;

	public function transform(object $item, TransformContext $ctx): Item;
}
