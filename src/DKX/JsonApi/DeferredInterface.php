<?php

declare(strict_types=1);

namespace DKX\JsonApi;

interface DeferredInterface
{
	/**
	 * @return mixed
	 */
	public function __invoke();
}
