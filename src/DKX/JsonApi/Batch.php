<?php

declare(strict_types=1);

namespace DKX\JsonApi;

final class Batch implements DeferredInterface
{
	private const IDS = 'ids';

	private const DATA = 'data';

	/** @var mixed[] */
	private static $storage = [];

	/** @var string */
	private $scope;

	/** @var callable */
	private $batchLoader;

	/** @var callable */
	private $itemLoader;

	/**
	 * @param mixed $id
	 */
	public function __construct(string $scope, $id, callable $batchLoader, callable $itemLoader)
	{
		$this->scope = $scope;
		$this->batchLoader = $batchLoader;
		$this->itemLoader = $itemLoader;

		if (!\array_key_exists($scope, self::$storage)) {
			self::$storage[$scope] = [
				self::IDS => [],
				self::DATA => null,
			];
		}

		self::$storage[$scope][self::IDS][] = $id;
	}

	public static function reset(): void
	{
		self::$storage = [];
	}

	public function __invoke()
	{
		$storage = &self::$storage[$this->scope];
		if ($storage[self::DATA] === null) {
			$storage[self::DATA] = \call_user_func($this->batchLoader, $storage[self::IDS]);
		}

		return \call_user_func($this->itemLoader, $storage[self::DATA]);
	}
}
