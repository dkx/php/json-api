<?php

declare(strict_types=1);

namespace DKX\JsonApi;

use DKX\JsonApi\Exception\InvalidArgumentException;
use DKX\JsonApi\Exception\InvalidStateException;
use DKX\JsonApiSerializer as Serializer;

final class Item
{
	public const ATTRIBUTES = 'attributes';

	public const RELATIONSHIPS = 'relationships';

	public const SELF_LINK = 'self_link';

	/** @var string */
	private $type;

	/** @var string */
	private $id;

	/** @var mixed[] */
	private $attributes;

	/** @var string|null */
	private $selfLink;

	/** @var mixed[] */
	private $relationships = [];

	/** @var callable[]|null */
	private $relationshipFactories;

	/** @var mixed[]|null */
	private $loadedRelationships;

	/**
	 * @param string $type
	 * @param string $id
	 * @param mixed[] $data
	 */
	public function __construct(string $type, string $id, array $data = [])
	{
		$this->type = $type;
		$this->id = $id;
		$this->attributes = $data[self::ATTRIBUTES] ?? [];
		$this->relationships = $data[self::RELATIONSHIPS] ?? [];
		$this->selfLink = $data[self::SELF_LINK] ?? null;
	}

	public function getType(): string
	{
		return $this->type;
	}

	public function getId(): string
	{
		return $this->id;
	}

	public function getSelfLink(): ?string
	{
		return $this->selfLink;
	}

	/**
	 * @return mixed[]
	 */
	public function getAttributes(): array
	{
		return $this->attributes;
	}

	/**
	 * @internal
	 * @param string[] $includes
	 */
	public function prefetchRelationships(Manager $manager, TransformContext $ctx, array $includes): void
	{
		$relationships = \array_filter($this->relationships, function ($key) use ($includes) {
			return \in_array($key, $includes, true);
		}, \ARRAY_FILTER_USE_KEY);

		$this->relationshipFactories = \array_map(function ($relationship) use ($manager, $ctx) {
			return $this->prefetchRelationship($manager, $ctx, $relationship);
		}, $relationships);
	}

	/**
	 * @internal
	 * @return Item[][]
	 */
	public function fetchRelationships(): array
	{
		if ($this->relationshipFactories === null) {
			throw new InvalidStateException('Can not fetch relationships because they were not prefetched yet');
		}

		$this->loadedRelationships = \array_map(function ($factory) {
			return $factory();
		}, $this->relationshipFactories);

		$dependentRelationships = \array_filter($this->loadedRelationships, function ($relationship) {
			return $relationship !== null;
		});

		$dependentRelationships = \array_map(function ($relationship) {
			if ($relationship instanceof Item) {
				return [$relationship];
			}

			return $relationship;
		}, $dependentRelationships);

		return $dependentRelationships;
	}

	/**
	 * @internal
	 */
	public function toJsonApiItem(): Serializer\Resource\Item
	{
		if ($this->loadedRelationships === null) {
			throw new InvalidStateException('Can not transform item to json api item because relationships are not fetched yet');
		}

		return new Serializer\Resource\Item($this->type, $this->id, [
			Serializer\Resource\Item::SELF_LINK => $this->selfLink,
			Serializer\Resource\Item::ATTRIBUTES => $this->attributes,
			Serializer\Resource\Item::RELATIONSHIPS => \array_map(function ($relationship): Serializer\Relationship\Relationship {
				return $this->createJsonApiRelationship($relationship);
			}, $this->loadedRelationships),
		]);
	}

	/**
	 * @param mixed $relationship
	 */
	private function prefetchRelationship(Manager $manager, TransformContext $ctx, $relationship): callable
	{
		if (\is_callable($relationship)) {
			$relationship = $relationship();
		}

		return function () use ($manager, $ctx, $relationship) {
			if ($relationship instanceof DeferredInterface) {
				$relationship = $relationship();
			}

			if ($relationship === null) {
				return null;
			}

			if (\is_object($relationship)) {
				return $manager->createItem($ctx, $relationship);
			}

			if (\is_array($relationship)) {
				return \array_map(function ($relationshipItem) use ($manager, $ctx) {
					if (!\is_object($relationshipItem)) {
						throw new InvalidArgumentException('Collection of relationships can contain only objects');
					}

					return $manager->createItem($ctx, $relationshipItem);
				}, $relationship);
			}

			throw new InvalidArgumentException('Unknown relationship type');
		};
	}

	/**
	 * @param mixed $relationship
	 */
	private function createJsonApiRelationship($relationship): Serializer\Relationship\Relationship
	{
		if ($relationship === null) {
			return new Serializer\Relationship\NullRelationship();
		}

		if ($relationship instanceof Item) {
			return new Serializer\Relationship\ItemRelationship(
				$relationship->toJsonApiItem()
			);
		}

		if (\is_array($relationship)) {
			return new Serializer\Relationship\CollectionRelationship(\array_map(function (Item $relationshipItem) {
				return new Serializer\Relationship\ItemRelationship(
					$relationshipItem->toJsonApiItem()
				);
			}, $relationship));
		}

		throw new InvalidArgumentException('Unknown relationship type');
	}
}
