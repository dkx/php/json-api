<?php

declare(strict_types=1);

namespace DKX\JsonApi;

final class Deferred implements DeferredInterface
{
	/** @var callable */
	private $loader;

	public function __construct(callable $loader)
	{
		$this->loader = $loader;
	}

	/**
	 * @return mixed
	 */
	public function __invoke()
	{
		return \call_user_func($this->loader);
	}
}
