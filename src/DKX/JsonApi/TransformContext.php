<?php

declare(strict_types=1);

namespace DKX\JsonApi;

final class TransformContext
{
	/** @var string[] */
	private $includes;

	/** @var mixed[] */
	private $attributes;

	/**
	 * @param string[] $includes
	 * @param mixed[] $attributes
	 */
	public function __construct(array $includes, array $attributes = [])
	{
		$this->includes = $includes;
		$this->attributes = $attributes;
	}

	/**
	 * @return string[]
	 */
	public function getIncludes(): array
	{
		return $this->includes;
	}

	/**
	 * @param string $name
	 * @param mixed|null $default
	 * @return mixed|null
	 */
	public function getAttribute(string $name, $default = null)
	{
		if (\array_key_exists($name, $this->attributes)) {
			return $this->attributes[$name];
		}

		return $default;
	}
}
